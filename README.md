Nume: Melinte Paul-Eduard  
Grupă: 336CC

# Tema 1 Marketplace

Toata cerinta temei a fost implementata

Producer:
    Threadurile producer sunt initiate cu daemon = True.
    Ele cicleaza prin lista lor de produse pana programul se inchide.
    Incearca sa adauge produsele din lista lor pana reusesc, asteptand cu sleep intre incercari.

Consumer:
    Threadurile consumer trec prin lista de carts asignata lor.
    Pentru fiecare cart, ruleaza comenzile date, de add sau remove, iar la final afiseaza lista in formatul dat.

Marketplace:
    In marketplace se salveaza dictionarul de reprezinta produsele publicate de producatori
    si lista de reprezinta cart-urile consumatorilor.
    Pentru id-urile consumatorilor si producatorilor se incrementeaza 2 variabile, cu cate
    un Lock pentru a garanta atomicitatea operatiei.
    Am ales sa pastrez coada produselor producatorilor intr-un dict de dict-uri, astfel coada
    unui producator este reprezentata prin perechi Product:cantitate.
    
https://gitlab.com/melinte.paul/asc-tema-1-marketplace
Git-ul este la momentul trimiterii temei privat, daca nu uit o sa il fac public pe 12 aprilie, dupa deadline-ul hard.