"""
This module represents the Consumer.

Computer Systems Architecture Course
Assignment 1
March 2021
"""

from threading import Thread
from time import sleep


class Consumer(Thread):
    """
    Class that represents a consumer.
    """

    def __init__(self, carts, marketplace, retry_wait_time, **kwargs):
        """
        Constructor.

        :type carts: List
        :param carts: a list of add and remove operations

        :type marketplace: Marketplace
        :param marketplace: a reference to the marketplace

        :type retry_wait_time: Time
        :param retry_wait_time: the number of seconds that a producer must wait
        until the Marketplace becomes available

        :type kwargs:
        :param kwargs: other arguments that are passed to the Thread's __init__()
        """

        Thread.__init__(self)

        self.cart_id = 0
        self.carts = carts
        self.marketplace = marketplace
        self.retry_wait_time = retry_wait_time
        self.name = kwargs.get('name', 'noname')

    def run(self):

        for cart in self.carts:
            self.cart_id = self.marketplace.new_cart()

            """
            Iteram prin fiecare cart
            In functie de comanda incercam sa adaugam sau sa returnam produsul pana reusim
            la final afisam produsele in formatul dat
            """
            for order in cart:
                prod_type = order['type']
                product = order['product']
                quantity = order['quantity']

                for _ in range(quantity):
                    if prod_type == 'add':
                        while not self.marketplace.add_to_cart(self.cart_id, product):
                            sleep(self.retry_wait_time)
                    elif prod_type == 'remove':
                        self.marketplace.remove_from_cart(self.cart_id, product)

                    sleep(self.retry_wait_time)

            for product in self.marketplace.place_order(self.cart_id):
                print(self.name + " bought " + str(product))
