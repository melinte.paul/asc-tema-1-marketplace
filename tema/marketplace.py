"""
This module represents the Marketplace.

Computer Systems Architecture Course
Assignment 1
March 2021
"""
from threading import Lock


class Marketplace:
    """
    Class that represents the Marketplace. It's the central part of the implementation.
    The producers and consumers use its methods concurrently.
    """

    def __init__(self, queue_size_per_producer):
        """
        Constructor

        :type queue_size_per_producer: Int
        :param queue_size_per_producer: the maximum size of a queue associated with each producer
        """

        self.q_size_max = queue_size_per_producer

        """
            Cream un lock pentru id-ul producatorilor pentru a nu avea duplicate sigur
            Fiecare producator are un q size, un dict de tip prod:prod_count si un lock
        """
        self.prod_no = 1
        self.reg_prod_lock = Lock()
        self.prod_locks = {}
        self.prod_published = {}
        self.prod_q_size = {}

        """
            Cream un producator default, id 0, in care adaugam cand dam remove din cos
        """
        self.prod_locks[0] = Lock()
        self.prod_published[0] = {}
        self.prod_q_size[0] = 0

        """
            Avem un loc pentru id-uri
            Fiecare cart_id are asociat o lista in carts
        """
        self.cart_no = 0
        self.new_cart_lock = Lock()
        self.carts = {}

    def register_producer(self):
        """
        Returns an id for the producer that calls this.

        Cream lock-ul producatorului, dict-ul pentru produse si cantitatea de produse
        este initializata la 0
        """
        with self.reg_prod_lock:
            self.prod_locks[self.prod_no] = Lock()
            self.prod_published[self.prod_no] = {}
            self.prod_q_size[self.prod_no] = 0
            self.prod_no += 1
            return self.prod_no - 1

    def publish(self, producer_id, product):
        """
        Adds the product provided by the producer to the marketplace

        :type producer_id: String
        :param producer_id: producer id

        :type product: Product
        :param product: the Product that will be published in the Marketplace

        :returns True or False. If the caller receives False, it should wait and then try again.

        Verificam daca mai avem spatiu in coada de produse
        Daca avem incrementam cantitatea produsului sau o adaugam in dict
        """

        with self.prod_locks[producer_id]:
            if self.prod_q_size[producer_id] == self.q_size_max:
                return False
            self.prod_published[producer_id][product] = self.prod_published[producer_id].get(product, 0) + 1
            self.prod_q_size[producer_id] += 1
            return True

    def new_cart(self):
        """
        Creates a new cart for the consumer

        :returns an int representing the cart_id

        Cream lista ce reprezinta cart-ul si incrementam id-ul
        """
        with self.new_cart_lock:
            self.carts[self.cart_no] = []
            self.cart_no += 1
            return self.cart_no - 1

    def add_to_cart(self, cart_id, product):
        """
        Adds a product to the given cart. The method returns

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to add to cart

        :returns True or False. If the caller receives False, it should wait and then try again

        Verificam daca un producator are produsul, daca il are il adaugam in cart
        Daca nu returnam False
        """

        for i in range(self.prod_no):
            with self.prod_locks[i]:
                if self.prod_published[i].get(product, 0) > 0:
                    self.carts[cart_id].append(product)
                    self.prod_published[i][product] = self.prod_published[i].get(product, 0) - 1
                    self.prod_q_size[i] -= 1
                    return True

        return False

    def remove_from_cart(self, cart_id, product):
        """
        Removes a product from cart.

        :type cart_id: Int
        :param cart_id: id cart

        :type product: Product
        :param product: the product to remove from cart

        Scoatem produsul din cart si il adaugam in producatorul default, id 0
        """

        self.carts[cart_id].remove(product)
        with self.prod_locks[0]:
            self.prod_published[0][product] = self.prod_published[0].get(product, 0) + 1

    def place_order(self, cart_id):
        """
        Return a list with all the products in the cart.

        :type cart_id: Int
        :param cart_id: id cart
        """

        return self.carts[cart_id]
